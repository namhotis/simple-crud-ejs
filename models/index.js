/* 
Definition
*/
    const Models = {
      post: require("./post.model"),
      user: require("./user.model"),
      like: require("./like.model"),
      comment: require("./comment.model"),
    }; 
//

/* 
Export
*/
    module.exports = Models;
// 
